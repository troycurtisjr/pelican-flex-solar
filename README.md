The minimalist [Pelican](http://blog.getpelican.com/) theme, [Solarized](http://ethanschoonover.com/solarized).

This theme is the awesome [Flex theme](https://github.com/alexandrevicenzi/Flex)
by
[Alexandre Vicenzi](https://blog.alexandrevicenzi.com/flex-pelican-theme.html),
[Solarized](https://gitlab.com/troycurtisjr/pelican-flex-solar) by
[Troy Curtis, Jr.](http://troycurtisjr.com). As this theme is effectively just a
different color scheme on top of the original, any improvements, updates, or
accolades should go to Alexandre and his Flex theme. Most of this page is the
same as the original Flex theme, but with a few bits changed to make clear this
is not the original.

I'm still working through making sure the color scheme looks good on all the
page types (since I'm not personally using all of them yet), so there may be a
few places that are still inconsistent. Feel free to let me know with an issue
or MR.

## Features

- Mobile First
- Responsive
- Semantic
- SEO Best Practices
- Open Graph
- Rich Snippets (JSON-LD)
- Related Posts (via [plugin](https://github.com/getpelican/pelican-plugins/tree/master/related_posts))
- Minute read like [Medium](https://medium.com/) (via [plugin](https://github.com/getpelican/pelican-plugins/tree/master/post_stats)) (new in 2.0)
- [Multiple Code Highlight Styles](https://github.com/alexandrevicenzi/Flex/wiki/Code-highlight-support)
- [Translation Support](https://github.com/alexandrevicenzi/Flex/wiki/Translation-support) (new in 2.0)

## Integrations

- [AddThis](http://www.addthis.com/)
- [Disqus](https://disqus.com/)
- [Gauges](http://get.gaug.es/)
- [Google AdSense](https://www.google.com.br/adsense/start/) (new in 2.1)
- [Google Analytics](https://www.google.com/analytics/web/)
- [Google Tag Manager](https://www.google.com/tagmanager/)
- [Piwik](http://piwik.org/)
- [StatusCake](https://www.statuscake.com/)

## Install

The recommend way to install is over [pelican-themes](https://github.com/getpelican/pelican-themes).

The `master` branch is the development branch. If you're happy with fresh new things and maybe sometimes (~most of time~) broken things you can clone the `master`, but I would recommend to you to clone a tag branch.

## Docs

[Go to Wiki](https://github.com/alexandrevicenzi/Flex/wiki)

## Contributing

Always open an issue before sending a PR. Talk about the problem/feature that you want to fix. If it's really a good thing you can submit your PR. If you send an PR without talking about before what it is, you may work for nothing.

As always, if you want something that only make sense to you, just fork Flex and start a new theme.

## Translate

Translate this theme to new languages at [Transifex](https://www.transifex.com/alexandrevicenzi/flex-pelican/).

![Translations](https://github.com/alexandrevicenzi/Flex/blob/master/translation_chart.png)

Read more about [Translation Support](https://github.com/alexandrevicenzi/Flex/wiki/Translation-support).

## Live example

I'm using Flex Solar on [my site](http://troycurtisjr.com).

You can see how things looks like with the original theme, look at Alexandre's blog [here](https://blog.alexandrevicenzi.com/flex-pelican-theme.html).
Or you can take a look at [Flex users](https://github.com/alexandrevicenzi/Flex/wiki/Flex-users).

Alexandre is using Flex on his [personal blog](http://blog.alexandrevicenzi.com/).

![Screenshot](https://github.com/alexandrevicenzi/Flex/blob/master/screenshot.png)

## Donate

Did you like this theme? Buy Alexandre a beer and support new features.

[![Gratipay](https://img.shields.io/gratipay/user/alexandrevicenzi.svg?maxAge=2592000)](https://gratipay.com/~alexandrevicenzi/)

[![PayPal](https://img.shields.io/badge/paypal-donate-yellow.svg?maxAge=2592000)](https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=KZCMH3N74KKFN&lc=BR&item_name=Alexandre%20Vicenzi&item_number=flex&currency_code=USD&bn=PP%2dDonationsBF%3abtn_donateCC_LG%2egif%3aNonHosted)

## License

MIT
